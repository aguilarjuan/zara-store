package com.inditex.zara.integration;

import com.inditex.zara.model.PricesEntity;
import com.inditex.zara.repository.PricesRepository;
import com.inditex.zara.utils.ParserUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Assert;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Disabled
public class PricesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PricesRepository pricesRepository;


    @DisplayName("get price on 2020-06-14 10:00:00")
    @Order(1)
    @Test
    public void find_price_whenMockMVC_thenReturns_20200614100000() throws Exception {

        this.mockMvc.perform(get("/api/v1/store/prices/")
                        .param("brandId", "1")
                        .param("productId", "35455")
                        .param("startDate", "2020-06-14 10:00:00"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.priceList").value(1))
                .andExpect(jsonPath("$.brandId").value(1))
                .andExpect(jsonPath("$.price").value(35.5))
                .andExpect(jsonPath("$.productId").value("35455"))
                .andExpect(jsonPath("$.startDate").value("2020-06-14 00:00:00"))
                .andExpect(jsonPath("$.endDate").value("2020-12-31 23:59:59"));

        List<PricesEntity> responseDB = this.pricesRepository.findPrice(1, ParserUtils.converterTimestamp("2020-06-14 10:00:00"), 35455);
        Assert.isTrue(responseDB.size() == 1, "unique result");
    }

    @DisplayName("get price on 2020-06-14 16:00:00")
    @Order(2)
    @Test
    public void find_price_whenMockMVC_thenReturns_20200614160000() throws Exception {

        this.mockMvc.perform(get("/api/v1/store/prices/")
                        .param("brandId", "1")
                        .param("productId", "35455")
                        .param("startDate", "2020-06-14 16:00:00"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.priceList").value(2))
                .andExpect(jsonPath("$.brandId").value(1))
                .andExpect(jsonPath("$.price").value(25.45))
                .andExpect(jsonPath("$.productId").value("35455"))
                .andExpect(jsonPath("$.startDate").value("2020-06-14 15:00:00"))
                .andExpect(jsonPath("$.endDate").value("2020-06-14 18:30:00"));

        List<PricesEntity> responseDB = this.pricesRepository.findPrice(1, ParserUtils.converterTimestamp("2020-06-14 16:00:00"), 35455);
        Assert.isTrue(responseDB.size() == 2, "multiples result");
    }

    @DisplayName("get price on 2020-06-14 21:00:00")
    @Order(3)
    @Test
    public void find_price_whenMockMVC_thenReturns_20200614210000() throws Exception {

        this.mockMvc.perform(get("/api/v1/store/prices/")
                        .param("brandId", "1")
                        .param("productId", "35455")
                        .param("startDate", "2020-06-14 21:00:00"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.priceList").value(1))
                .andExpect(jsonPath("$.brandId").value(1))
                .andExpect(jsonPath("$.price").value(35.5))
                .andExpect(jsonPath("$.productId").value("35455"))
                .andExpect(jsonPath("$.startDate").value("2020-06-14 00:00:00"))
                .andExpect(jsonPath("$.endDate").value("2020-12-31 23:59:59"));

        List<PricesEntity> responseDB = this.pricesRepository.findPrice(1, ParserUtils.converterTimestamp("2020-06-14 21:00:00"), 35455);
        Assert.isTrue(responseDB.size() == 1, "unique result");
    }

    @DisplayName("get price on 2020-06-15 10:00:00")
    @Order(4)
    @Test
    public void find_price_whenMockMVC_thenReturns_20200615100000() throws Exception {

        this.mockMvc.perform(get("/api/v1/store/prices/")
                        .param("brandId", "1")
                        .param("productId", "35455")
                        .param("startDate", "2020-06-15 10:00:00"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.priceList").value(3))
                .andExpect(jsonPath("$.brandId").value(1))
                .andExpect(jsonPath("$.price").value(30.5))
                .andExpect(jsonPath("$.productId").value("35455"))
                .andExpect(jsonPath("$.startDate").value("2020-06-15 00:00:00"))
                .andExpect(jsonPath("$.endDate").value("2020-06-15 11:00:00"));

        List<PricesEntity> responseDB = this.pricesRepository.findPrice(1, ParserUtils.converterTimestamp("2020-06-15 10:00:00"), 35455);
        Assert.isTrue(responseDB.size() == 2, "multiples result");
    }

    @DisplayName("get price on 2020-06-16 21:00:00")
    @Order(5)
    @Test
    public void find_price_whenMockMVC_thenReturns_20200616210000() throws Exception {

        this.mockMvc.perform(get("/api/v1/store/prices/")
                        .param("brandId", "1")
                        .param("productId", "35455")
                        .param("startDate", "2020-06-16 21:00:00"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.priceList").value(4))
                .andExpect(jsonPath("$.brandId").value(1))
                .andExpect(jsonPath("$.price").value(38.95))
                .andExpect(jsonPath("$.productId").value("35455"))
                .andExpect(jsonPath("$.startDate").value("2020-06-15 16:00:00"))
                .andExpect(jsonPath("$.endDate").value("2020-12-31 23:59:59"));

        List<PricesEntity> responseDB = this.pricesRepository.findPrice(1, ParserUtils.converterTimestamp("2020-06-16 21:00:00"), 35455);
        Assert.isTrue(responseDB.size() == 2, "multiples result");
    }

}
