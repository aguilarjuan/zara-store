package com.inditex.zara.unit;

import com.inditex.zara.dto.PriceDto;
import com.inditex.zara.exeption.StoreBusinessException;
import com.inditex.zara.mapper.PriceMapper;
import com.inditex.zara.model.PricesEntity;
import com.inditex.zara.repository.PricesRepository;
import com.inditex.zara.service.impl.PriceServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PriceServiceImplTest {

    @Mock
    PricesRepository pricesRepository;

    @Mock
    PriceMapper priceMapper;

    @InjectMocks
    PriceServiceImpl priceServiceImpl;

    @Test
    void testGetPriceService() throws StoreBusinessException {

        PricesEntity PricesEntityOne = new PricesEntity(1L, 1, Timestamp.valueOf("2020-06-14 00:00:00"), Timestamp.valueOf("2020-12-31 23:59:59"), 35455L, 0, 35.5, "EUR", "2020-06-13 00:00:00", "jucaguilar");
        PricesEntity PricesEntityTwo = new PricesEntity(4L, 1, Timestamp.valueOf("2020-06-15 16:00:00"), Timestamp.valueOf("2020-12-31 23:59:59"), 35455L, 1, 38.95, "EUR", "2020-06-13 00:00:00", "jucaguilar");

        List<PricesEntity> responseMock = new ArrayList<>();
        responseMock.add(PricesEntityOne);
        responseMock.add(PricesEntityTwo);

        PriceDto priceDtoOne = PriceDto.builder().priceList(1L).brandId(1).startDate("2020-06-14 00:00:00").endDate("2020-12-31 23:59:59").productId(35455L).priority(0).price(35.5).currency("EUR").lastUpdate("2020-06-13 00:00:00").lastUpdateBy("jucaguilar").build();
        PriceDto priceDtoTwo = PriceDto.builder().priceList(4L).brandId(1).startDate("2020-06-15 16:00:00").endDate("2020-12-31 23:59:59").productId(35455L).priority(1).price(38.95).currency("EUR").lastUpdate("2020-06-13 00:00:00").lastUpdateBy("jucaguilar").build();

        List<PriceDto> mapperMock = new ArrayList<>();
        mapperMock.add(priceDtoOne);
        mapperMock.add(priceDtoTwo);

        when(pricesRepository.findPrice(anyInt(), any(), anyLong())).thenReturn(responseMock);

        when(priceMapper.entityToDtoList(any())).thenReturn(mapperMock);

        PriceDto result = priceServiceImpl.getPriceService(1, "2020-06-16 21:00:00", 35455L);
        Assertions.assertEquals(priceDtoTwo, result);
    }
}