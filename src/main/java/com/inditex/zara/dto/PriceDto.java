package com.inditex.zara.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PriceDto {

    private Long priceList;
    private Integer brandId;
    private String startDate;
    private String endDate;
    private Long productId;
    private Integer priority;
    private Double price;
    private String currency;
    private String lastUpdate;
    private String lastUpdateBy;
}
