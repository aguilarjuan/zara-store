package com.inditex.zara;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZaraStoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZaraStoreApplication.class, args);
    }

}
