package com.inditex.zara.utils;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;

public class ParserUtils {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static String convert(Timestamp ts) {
        return ts.toLocalDateTime().format(FORMATTER);
    }

    public static Timestamp converterTimestamp(String date) {
        return Timestamp.valueOf(date);
    }
}
