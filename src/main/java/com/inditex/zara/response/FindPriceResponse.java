package com.inditex.zara.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FindPriceResponse {

    private Long priceList;
    private Long productId;
    private Integer brandId;
    private String startDate;
    private String endDate;
    private Double price;
}
