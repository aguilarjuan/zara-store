package com.inditex.zara.service.impl;

import com.inditex.zara.dto.PriceDto;
import com.inditex.zara.exeption.NotFoundEntity;
import com.inditex.zara.exeption.StoreBusinessException;
import com.inditex.zara.mapper.PriceMapper;
import com.inditex.zara.repository.PricesRepository;
import com.inditex.zara.service.PriceService;
import com.inditex.zara.utils.ParserUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class PriceServiceImpl implements PriceService {

    private final PricesRepository pricesRepository;

    private final PriceMapper priceMapper;

    @Override
    public PriceDto getPriceService(int brandId, String startDate, long productId) throws StoreBusinessException {
        try {
            List<PriceDto> response = priceMapper.entityToDtoList(this.pricesRepository.findPrice(brandId, ParserUtils.converterTimestamp(startDate), productId));
            verifyResult(response);
            log.info("query [getPriceService] number results is {}", response.size());
            if (response.size() == 1) {
                return response.get(0);
            } else {
                return getPriceFilter(response);
            }
        } catch (Exception e) {
            log.error("unknown error invoking service [getPriceService]");
            throw new StoreBusinessException(StoreBusinessException.SERVICE_GET_PRICES, e.getMessage());
        }
    }

    private PriceDto getPriceFilter(List<PriceDto> response) throws NotFoundEntity {
        List<PriceDto> filter = response.stream().filter(x -> x.getPriority() == 1).collect(Collectors.toList());
        if (filter.size() == 1) {
            return filter.get(0);
        } else {
            log.error("error query [getPriceService] exist multiples results");
            throw new NotFoundEntity(NotFoundEntity.MULTIPLES_ENTITIES);
        }
    }

    private void verifyResult(List<PriceDto> response) throws NotFoundEntity {
        if (response.size() == 0) {
            log.error("error query [getPriceService] not found result");
            throw new NotFoundEntity(NotFoundEntity.NOT_FOUND_ENTITY);
        }
    }
}
