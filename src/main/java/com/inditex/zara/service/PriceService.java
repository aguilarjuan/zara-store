package com.inditex.zara.service;

import com.inditex.zara.dto.PriceDto;
import com.inditex.zara.exeption.StoreBusinessException;

public interface PriceService {

    PriceDto getPriceService(int brandId, String startDate, long productId) throws StoreBusinessException;

}
