package com.inditex.zara.controller;

import com.inditex.zara.dto.PriceDto;
import com.inditex.zara.exeption.StoreBusinessException;
import com.inditex.zara.mapper.PriceMapper;
import com.inditex.zara.response.FindPriceResponse;
import com.inditex.zara.service.PriceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@RestController
@Validated
@RequiredArgsConstructor
@Slf4j
@RequestMapping("api/v1/store")
public class PricesController {

    private final PriceService priceService;

    private final PriceMapper priceMapper;

    @GetMapping(path = "/prices", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FindPriceResponse> getPrice(@RequestParam("brandId") @NotNull int brandId,
                                                      @RequestParam("startDate") @NotEmpty String startDate,
                                                      @RequestParam("productId") @NotNull long productId)
            throws StoreBusinessException {
        log.info("invokin service prices with data: brandId {}, startDate {}, productId {}", brandId, startDate, productId);
        PriceDto response = this.priceService.getPriceService(brandId, startDate, productId);
        log.info("response service: {} ", response.toString());
        return new ResponseEntity<>(this.priceMapper.buildFindPriceResponse(response), HttpStatus.OK);
    }
}
