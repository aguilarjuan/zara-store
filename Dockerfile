FROM adoptopenjdk/openjdk11:latest
RUN mkdir /usr/src/zaradocker
COPY ./target/zara-0.0.1-SNAPSHOT.jar /usr/src/zaradocker
WORKDIR /usr/src/zaradocker
EXPOSE 8080
CMD ["java", "-jar", "/usr/src/zaradocker/zara-0.0.1-SNAPSHOT.jar"]
