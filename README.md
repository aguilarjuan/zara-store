# zara store service API REST: find prices

## Índice

El proyecto ***zara store*** utiliza el patron de arquitectura MVC

## Instalación

- se puede clonar el proyecto desde github : git clone https://aguilarjuan@bitbucket.org/aguilarjuan/zara-store.git
- usted puede instalar una imagen docker del proyecto ejecutando el comando : docker build -t zara:v1 .
- luego se creara un contenedor docker de la aplicacion que escuchara en la
  direccion: http://localhost:8080/api/v1/store/prices
- la mejor forma para probar la API rest es utilizando la herramienta Postman, usted puede utilizar directamente
- ya existe una casuistica de prueba que se encuentra en la ruta interna: ***src/main/resources/static/ usted solo debe
  exportarlo a la herramienta Postman

- [acceso a la base de la aplicacion] la aplicacion utiliza una base en memoria (H2) para persistir los datos

- [acceso a la documentacion de la API RESt]  http://localhost:8080/swagger-ui.html#/



